/**
  @file    read-sectors.ino
  @author
    AMOUSSOU Z. Kenneth 33972
    Hatem Ahmed
  @brief   Read all the sectors of the MF1S50 card

  @date 03.11.2021
  @section  HISTORY

  V1.0 initial version
*/

/** include library */
#include <Wire.h>
#include "nfc.h"

/** define a nfc class */
NFC_Module nfc;

void setup(void)
{
  Serial.begin(9600);
  nfc.begin();
  Serial.println("MF1S50 Memory Reader");

  uint32_t versiondata = nfc.get_version();
  if (! versiondata) {
    Serial.print("Didn't find PN53x board");
    while (1); // halt
  }

  // Got ok data, print it out!
  Serial.print("Found chip PN5"); Serial.println((versiondata>>24) & 0xFF, HEX);
  Serial.print("Firmware ver. "); Serial.print((versiondata>>16) & 0xFF, DEC);
  Serial.print('.'); Serial.println((versiondata>>8) & 0xFF, DEC);

  /** Set normal mode, and disable SAM */
  nfc.SAMConfiguration();

  delay(500);

  /* Memory dump */

  Serial.print("--------------------------- Start of memory dump ");
  Serial.println("---------------------------");
  u8 buf[32], sta;

  /** Polling the mifar card, buf[0] is the length of the UID */
  sta = nfc.InListPassiveTarget(buf);

  /** check state and UID length */
  if(sta && buf[0] == 4){
    /** the card may be Mifare Classic card, try to read the block */
    Serial.print("UUID length:");
    Serial.print(buf[0], DEC);
    Serial.println();
    Serial.print("UUID:");
    nfc.puthex(buf+1, buf[0]);
    Serial.println();
    /** factory default KeyA: 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF */
    u8 key[6] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};


    for (u8 sectornum = 0; sectornum < 16; sectornum += 1) {
      sta = nfc.MifareAuthentication(0, 4 * sectornum, buf+1, buf[0], key);
      if (sta) {
        Serial.print("--------------------------- Sector ");
        Serial.print(sectornum);
        Serial.println(" ---------------------------");
        /** save read block data */
        u8 block[16];

        for (u8 blocknum = 0; blocknum < 4; blocknum++) {
          sta = nfc.MifareReadBlock(4 * sectornum + blocknum, block);
          if(sta){
            Serial.print("Block ");
            Serial.print(4 * sectornum + blocknum);
            Serial.print(" ");
            nfc.puthex(block, 16);
            Serial.println();
          }
        }
      } else {
        Serial.println("Authentication failed for Sector " + String(sectornum));
      }
    }
  }

}

void loop(void) { delay(100); }
