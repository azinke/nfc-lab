/**
  @file    rw-tag.ino
  @author
    AMOUSSOU Z. Kenneth 33972
    Hatem Ahmed
  @brief   Read/Write a sector of the MF1S50 card
  @date 11.11.2021
  @version 0.1
*/

#include "nfc.h"

// Sector to read from and to write to
#define ID_SECTOR 1

// The leeway is the minimum time between two consecutive detections
// It's supposed to prevent multiple detection (and also prevent from
// wasting write cycles)
#define LEEWAY  5000  // ms

// UUID of the tag for testing
// In a production code, the UUID of the tag should
// not be hard-coded.
// Therefore, this is only for the lab purpose
const u8 TAG_UUID[] = {0x69, 0x2B, 0x14, 0x10};

// define a nfc class
NFC_Module nfc;

volatile unsigned long long counter = 0;

// The lock is used to control read and write operations
bool is_tag_locked = false;

void setup(void) {
  // Init serial communication
  Serial.begin(9600);

  nfc.begin();
  uint32_t versiondata = nfc.get_version();
  if (! versiondata) {
    Serial.print("PN53x board not found");
    while (1); // halt
  }

  // Got ok data, print it out!
  Serial.print("Found chip PN5"); Serial.println((versiondata>>24) & 0xFF, HEX);
  Serial.print("Firmware ver. "); Serial.print((versiondata>>16) & 0xFF, DEC);
  Serial.print('.'); Serial.println((versiondata>>8) & 0xFF, DEC);
  Serial.println();

  // Set normal mode, and disable SAM
  nfc.SAMConfiguration();

  // initialize timer 2
  // Note: Timer 2 is being use for the counter
  initTimer2();
}

void loop(void) {
  u8 buf[32], sta;
  switch (authenticate(buf)) {
    case 0: {
      // Authentication failed
      Serial.println("Authentication failed!");
      break;
    }
    case 1: {
      // Authentication successful
      Serial.println("Authentication successful!");
      if (!is_tag_locked) {
        counter = read_id();
        is_tag_locked = true;
        Serial.println("Read ID: " + String((unsigned long)counter));
      } else {
        write_id(counter);
        // release the lock
        is_tag_locked = false;
      }
      delay(LEEWAY);
      break;
    }
    case -1: {
      // Not detected or wrong tag detected
      break;
    }
    default: {}
      // other cases
  }

  delay(100);
}

/**
 * \brief Check tag UUID 
 *  The UUID of the tag detected is matched with the one provided
 * to us for the lab.
 *
 * NOTE: Not a production ready code sample.
 *
 * @return Boolean flag indicating matching tag UUID
 */
bool is_test_tag(u8 *buf){
  // UUID lenght expected must is four
  if (buf[0] == 4) {
    for (u8 i = 0; i < 4; i++) {
      if (buf[i + 1] != TAG_UUID[i]) return false;
    }
    return true;
  } else return false;
}

/**
 * \brief Authentication routine
 *
 * @return
 * -1: No tag detected or Wrong tag (UUID not matching) - @see is_test_tag
 *  0: Authentication failed
 *  1: Authentication successful
 */
int8_t authenticate(u8 *buf) {
  u8 sta;
  // factory default KeyA: 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF
  u8 key[6] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};

  /** Polling the mifar card, buf[0] is the length of the UID */
  sta = nfc.InListPassiveTarget(buf);
  if(sta && is_test_tag(buf)){
    sta = nfc.MifareAuthentication(0, 4 * ID_SECTOR, buf+1, buf[0], key);
    if (sta) return 1;
    else return 0;
  }
  return -1;
}

/**
 * \brief Read the current value in a given sector of the tag
 *
 * The ID is supposed to be in the first block of the sector.
 *
 * @return ID
 *         The ID is a 8 bytes integer (long long - 64 bits)
 *
 * NOTE: Does not perform authentication. So, it's importanrt to
 * handle authentication before calling this function.
 */
unsigned long long read_id() {
  u8 sta;
  // save read block data
  u8 block[16];
  sta = nfc.MifareReadBlock(4 * ID_SECTOR, block);
  if (sta) {
    unsigned long long id = 0;
    // Contruct the ID by shifting bytes
    // The first bytes being the least significant
    for (u8 idx = 0; idx < 8; idx++) {
      id |= (unsigned long long)block[idx] << (idx * 8);
    }
    return id;
  }
  return 0;
}

/**
 * \brief Write an ID in a given block of the tag
 *
 * The ID is supposed to be in the first block of the sector
 *
 * @param id Value of the counte to write
 *
 * @return Boolean flag indicating the successful completion of the
 * write operation or not.
 *
 * NOTE: Does not perform authentication. So, it's importanrt to
 * handle authentication before calling this function.
 */
bool write_id(unsigned long long id) {
  u8 sta;
  // save read block data
  u8 block[16];
  // The read read allow us to keep the bytes of the block
  // that does not belong to the ID intact
  // Otherwise, if some relevant data is stored in those bytes,
  // we might lost them
  sta = nfc.MifareReadBlock(4 * ID_SECTOR, block);
  if (!sta) {
    Serial.println("Cannot read block.");
    return;
  }
  for (u8 idx = 0; idx < 8; idx++) {
    unsigned long long id_mask = id & ((unsigned long long)0xFF << (idx * 8));
    block[idx] = (u8)(id_mask >> (idx * 8));
  }
  sta = nfc.MifareWriteBlock(4 * ID_SECTOR, block);
  if (sta) {
    Serial.println("ID " + String((unsigned long)id) + " written!");
    return true;
  }
  else {
    Serial.println("Write operation failed!");
    return false;
  }
}

/**
 * \brief configure the timer 2 module of the ATMEGA328 µC
 *  The timer is configured to trigger every millisecond
 * @return: none
 */
void initTimer2(){
  cli();
  TCCR2A = 0x00;
  ASSR &= ~(1<<5); // clk_io
  /**
    Fosc = 16MHz
    prescaler = 1024
    256 steps -> 16.384 ms
    16 steps  -> 1.024 ms
    32 steps  -> 2.048 ms (~ 488.28125 Hz)
    47 steps  -> 3.008 ms (~ 332.4468085 Hz) **
    63 steps  -> 4.032 ms (~ 248.015873 Hz) 
  **/
  TCNT2 = (255 - 16);
  GTCCR &= ~(1<<7);   // disable timer 2 prescaler reset
  TCCR2B = 0x07;
  TIFR2 = 0x00;
  TIMSK2 = 0x01;
  sei();
}

/**
 * \brief Interrupt Service Routine
 * @vector: timer 2 vector
 */
SIGNAL(TIMER2_OVF_vect){
  cli();                      // disable interrupt
  TIFR2 = 0x00;               // clear interrupt flag
  counter++;
  TCNT2 = (255 - 16);         // reset timer2 counter register
  sei();                      // enable interrupt
}
